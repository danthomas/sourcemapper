using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace SourceMapper.Tests
{
    public class TestBase
    {
        protected static void ExceptionTest(Mapper<Thing> mapper, ISource source, string expected)
        {
            string actual = "";

            try
            {
                mapper.MapItem(source);
            }
            catch (Exception e)
            {
                actual = e.Message;
            }

            Assert.AreEqual(expected, actual);
        }

        protected static void MapItemTest(Mapper<Thing> mapper, ISource source, string expected)
        {
            var actual = JsonConvert.SerializeObject(mapper.MapItem(source), Formatting.Indented, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            }).Replace("\"", "'");
            Assert.AreEqual(expected, actual);
        }

        protected static void MapItemsTest(Mapper<Thing> mapper, ISource source, string expected)
        {
            var actual = JsonConvert.SerializeObject(mapper.MapItems(source), Formatting.Indented, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            }).Replace("\"", "'");
            Assert.AreEqual(expected, actual);
        }
    }
}