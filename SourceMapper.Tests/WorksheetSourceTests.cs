using System.IO;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OfficeOpenXml;

namespace SourceMapper.Tests
{
    [TestClass]
    public class WorksheetSourceTests : TestBase
    {
        [TestMethod]
        public void MapItem()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "SourceMapper.Tests.Spreadsheet.xlsx";
            WorksheetSource source;

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                var package = new ExcelPackage(stream);
                source = new WorksheetSource(package.Workbook.Worksheets["Worksheet1"]);
            }

            var mapper = new Mapper<Thing>()
                .For(x => x.StringProp).MapFromRightOf("StringProp")
                .For(x => x.IntProp).MapFromRightOf("IntProp")
                .For(x => x.BoolProp).MapFromRightOf("BoolProp")
                .Build();

            MapItemTest(mapper, source, @"{
  'StringProp': 'ABC',
  'IntProp': 123,
  'BoolProp': true
}");
        }

        [TestMethod]
        public void MapItems()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "SourceMapper.Tests.Spreadsheet.xlsx";
            WorksheetSource source;

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                var package = new ExcelPackage(stream);
                source = new WorksheetSource(package.Workbook.Worksheets["Worksheet1"]);
            }

            var mapper = new Mapper<Thing>()
                .For(x => x.StringProp).MapFromBelow("StringColumn")
                .For(x => x.IntProp).MapFromBelow("IntColumn")
                .For(x => x.BoolProp).MapFromBelow("BoolColumn").Pre(x => x.ToLower() == "yes" ? "true" : "false")
                .Build();

            MapItemsTest(mapper, source, @"[
  {
    'StringProp': 'Abc',
    'IntProp': 123,
    'BoolProp': true
  },
  {
    'StringProp': 'Def',
    'IntProp': 456,
    'BoolProp': false
  },
  {
    'StringProp': 'Ghi',
    'IntProp': 798,
    'BoolProp': true
  }
]");
        }
    }
}