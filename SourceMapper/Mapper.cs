using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace SourceMapper
{
    public class Mapper<T> where T : new()
    {
        public class Intermediate<P>
        {
            private readonly Mapper<T> _mapper;
            private readonly Expression<Func<T, P>> _prop;

            public Intermediate(Mapper<T> mapper, Expression<Func<T, P>> prop)
            {
                _mapper = mapper;
                _prop = prop;
            }

            public ValueMapping<P> UseValue(P value)
            {
                var mapping = new ValueMapping<P>(_mapper, _prop, value);
                _mapper._mappings.Add(mapping);
                return mapping;
            }

            public TransformMapping<P> MapFromRightOf(string name)
            {
                var mapping = new TransformMapping<P>(_mapper, _prop, name, Position.Right);
                _mapper._mappings.Add(mapping);
                return mapping;
            }

            public TransformMapping<P> MapFromBelow(string name)
            {
                var mapping = new TransformMapping<P>(_mapper, _prop, name, Position.Below);
                _mapper._mappings.Add(mapping);
                return mapping;
            }
        }

        private readonly List<IMapping> _mappings;

        public Mapper()
        {
            _mappings = new List<IMapping>();
        }

        public Intermediate<P> For<P>(Expression<Func<T, P>> prop)
        {
            return new Intermediate<P>(this, prop);
        }

        public T MapItem(ISource source)
        {
            return MapItemPrivate(source);
        }

        public List<T> MapItems(ISource source)
        {
            List<T> list = new List<T>();

            var offset = 1;

            while (_mappings.Any(x => !String.IsNullOrWhiteSpace(source.GetValue(x.Name, x.Position, offset))))
            {
                list.Add(MapItemPrivate(source, offset));

                offset++;
            }

            return list;
        }

        private T MapItemPrivate(ISource source, int offset = 1)
        {
            var t = new T();

            foreach (var mapping in _mappings)
            {
                mapping.SetValue(t, source, offset);
            }

            return t;
        }

        internal interface IMapping
        {
            void SetValue(T t, ISource source, int offset = 1);
            string Name { get; }
            Position Position { get; }
        }

        public class TransformMapping<P> : Mapping<P>
        {
            private P _defaultValue;
            private bool _defaultValueSet;
            private readonly List<Func<string, string>> _preFuncs;
            private readonly List<Func<P, P>> _postFuncs;
            private bool _optional;
            private Delegate _delegate;
            private bool _nullable;

            public TransformMapping(Mapper<T> mapper, Expression<Func<T, P>> func, string name, Position position)
                : base(mapper, func, name, position)
            {
                _preFuncs = new List<Func<string, string>>();
                _postFuncs = new List<Func<P, P>>();

                var param = Expression.Parameter(typeof(string));

                var type = typeof(P);
                if (type == typeof(string))
                {
                    var lambda = Expression.Lambda(param, param);

                    _delegate = lambda.Compile();
                }
                else if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    _nullable = true;

                    var genericType = type.GetGenericArguments()[0];

                    var methodInfo = genericType.GetMethod("Parse", BindingFlags.Static | BindingFlags.Public, null, new[] { typeof(string) }, null);

                    var parse = Expression.Call(methodInfo, param);

                    var constructor = type.GetConstructor(BindingFlags.Public | BindingFlags.Instance, null, new[] { genericType }, null);

                    var newNullable = Expression.New(constructor, parse);

                    var lambda = Expression.Lambda(newNullable, param);

                    _delegate = lambda.Compile();
                }
                else
                {
                    var methodInfo = type.GetMethod("Parse", BindingFlags.Static | BindingFlags.Public, null, new[] { typeof(string) }, null);

                    var parse = Expression.Call(methodInfo, param);

                    var lambda = Expression.Lambda(parse, param);

                    _delegate = lambda.Compile();
                }
            }

            public override void SetValue(T t, ISource source, int offset = 1)
            {
                var value = source.GetValue(Name, Position, offset);

                foreach (var preFunc in _preFuncs)
                {
                    value = preFunc(value);
                }

                P p = default(P);

                if (string.IsNullOrWhiteSpace(value))
                {
                    if (_defaultValueSet)
                    {
                        p = _defaultValue;
                    }
                    else if (!_optional && !_nullable)
                    {
                        throw new Exception($"No value found for non optional non nullable {Name} mapping and no default set.");
                    }
                }
                else
                {
                    try
                    {
                        p = (P)_delegate.DynamicInvoke(value);
                    }
                    catch
                    {
                        throw new Exception($"Failed to convert value {value} to {typeof(P).Name}");
                    }
                }

                foreach (var postFunc in _postFuncs)
                {
                    p = postFunc(p);
                }

                SetFunc(t, p);
            }

            public TransformMapping<P> WithDefault(P defaultValue)
            {
                _defaultValueSet = true;
                _defaultValue = defaultValue;
                return this;
            }

            public TransformMapping<P> Optional()
            {
                _optional = true;
                return this;
            }

            public TransformMapping<P> Pre(Func<string, string> preFunc)
            {
                _preFuncs.Add(preFunc);
                return this;
            }

            public TransformMapping<P> Post(Func<P, P> postFunc)
            {
                _postFuncs.Add(postFunc);
                return this;
            }

            public TransformMapping<P> Trim()
            {
                _preFuncs.Add(x => x.Trim());
                return this;
            }

            public TransformMapping<P> ToUpper()
            {
                _preFuncs.Add(x => x.ToUpper());
                return this;
            }

            public TransformMapping<P> ToLower()
            {
                _preFuncs.Add(x => x.ToLower());
                return this;
            }

            public TransformMapping<P> RemoveLeading(params string[] texts)
            {
                _preFuncs.Add(x =>
                {
                    foreach (var text in texts)
                    {
                        if (x.StartsWith(text))
                        {
                            return x.Substring(text.Length);
                        }
                    }

                    return x;
                });
                return this;
            }

            public TransformMapping<P> RemoveTrailing(params string[] texts)
            {
                _preFuncs.Add(x =>
                {
                    foreach (var text in texts)
                    {
                        if (x.EndsWith(text))
                        {
                            return x.Substring(0, x.Length - text.Length);
                        }
                    }

                    return x;
                });
                return this;
            }
        }

        public class ValueMapping<P> : Mapping<P>
        {
            private readonly P _value;

            public ValueMapping(Mapper<T> mapper, Expression<Func<T, P>> func, P value)
                : base(mapper, func, null, Position.None)
            {
                _value = value;
            }

            public override void SetValue(T t, ISource source, int offset = 1)
            {
                SetFunc(t, _value);
            }
        }

        public abstract class Mapping<P> : IMapping
        {
            public string Name { get; }
            public Position Position { get; }
            protected readonly Mapper<T> Mapper;
            protected readonly Func<T, P, P> SetFunc;

            protected Mapping(Mapper<T> mapper, Expression<Func<T, P>> func, string name, Position position)
            {
                Name = name;
                Position = position;
                Mapper = mapper;
                Stack<MemberExpression> memberExpressions = new Stack<MemberExpression>();
                var xxx = (MemberExpression)func.Body;
                memberExpressions.Push(xxx);

                while (xxx.Expression.NodeType == ExpressionType.MemberAccess)
                {
                    xxx = (MemberExpression)xxx.Expression;
                    memberExpressions.Push(xxx);
                }

                ParameterExpression parameterExpression = Expression.Parameter(typeof(T));

                Expression expression = parameterExpression;
                List<Expression> expressions = new List<Expression>();

                while (memberExpressions.Any())
                {
                    var memberExpression = memberExpressions.Pop();

                    var memberExpressionMember = (PropertyInfo)memberExpression.Member;

                    expression = Expression.Property(expression, memberExpressionMember);

                    if (memberExpressions.Any())
                    {
                        expressions.Add(Expression.IfThen(Expression.Equal(expression, Expression.Constant(null, expression.Type)),
                            Expression.Assign(expression, Expression.New(expression.Type.GetConstructor(new Type[0])))));
                    }
                }

                ParameterExpression valueExpression = Expression.Parameter(typeof(P));

                expressions.Add(Expression.Assign(expression, valueExpression));

                var blockExpression = Expression.Block(expressions);

                SetFunc = Expression.Lambda<Func<T, P, P>>(
                    blockExpression, parameterExpression, valueExpression).Compile();
            }

            public abstract void SetValue(T t, ISource source, int offset = 1);

            public Intermediate<N> For<N>(Expression<Func<T, N>> prop)
            {
                return Mapper.For(prop);
            }

            public Mapper<T> Build()
            {
                return Mapper;
            }
        }

        public Mapper<T> Required()
        {
            return this;
        }

        public Mapper<T> MapFrom(string name)
        {
            return this;
        }
    }
}