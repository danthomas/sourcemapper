using System;
using System.Collections.Generic;
using System.Linq;

namespace SourceMapper
{
    public class GridSource : ISource
    {
        private readonly string[,] _data;
        private readonly Dictionary<string, Cell> _nameCells;
        public GridSource(string[,] data)
        {
            _data = data;
            _nameCells = new Dictionary<string, Cell>();
        }

        public string GetValue(string name, Position position = Position.Right, int offset = 1)
        {
            var cell = GetNameCell(name);

            switch (position)
            {
                case Position.Right:
                    cell.X += offset;
                    break;
                case Position.Below:
                    cell.Y += offset;
                    break;
            }

            if (cell.X < 0 || cell.X >= _data.GetLength(0) || cell.Y < 0 || cell.Y >= _data.GetLength(1))
            {
                return null;
            }

            return _data[cell.X, cell.Y];
        }

        private Cell GetNameCell(string name)
        {
            if (_nameCells.ContainsKey(name))
            {
                return _nameCells[name];
            }

            var cells = new List<Cell>();

            for (var x = 0; x < _data.GetLength(0); x++)
            {
                for (var y = 0; y < _data.GetLength(1); y++)
                {
                    if (_data[x, y] == name)
                    {
                        cells.Add(new Cell { X = x, Y = y });
                    }
                }
            }

            if (cells.Count > 1)
            {
                throw new Exception($"Multiple cells for name: {name} found");
            }

            if (cells.Count == 0)
            {
                throw new Exception($"Multiple cells for name: {name} found");
            }

            return cells.First();
        }

        class Cell
        {
            public int Y { get; set; }
            public int X { get; set; }
        }
    }
}