using OfficeOpenXml;

namespace SourceMapper
{
    public class WorksheetSource : GridSource
    {
        public WorksheetSource(ExcelWorksheet worksheet)
            : this(GenData(worksheet))
        {
        }

        private WorksheetSource(string[,] data)
            : base(data)
        {
        }

        private static string[,] GenData(ExcelWorksheet worksheet)
        {
            var objectData = worksheet.Cells.GetValue<object[,]>();
            var data = new string[objectData.GetLength(1), objectData.GetLength(0)];
            for (var x = 0; x < objectData.GetLength(1); x++)
            {
                for (var y = 0; y < objectData.GetLength(0); y++)
                {
                    data[x, y] = objectData[y, x]?.ToString() ?? "";
                }
            }

            return data;
        }
    }
}