using System.Collections.Generic;

namespace SourceMapper
{
    public class Source : ISource
    {
        private readonly Dictionary<string, string> _dictionary;

        public Source()
        {
            _dictionary = new Dictionary<string, string>();
        }

        public Source SetValue(string name, string value)
        {
            _dictionary.Add(name, value);
            return this;
        }

        public string GetValue(string name, Position position = Position.Right, int offset = 0)
        {
            return _dictionary.ContainsKey(name)
                ? _dictionary[name]
                : null;
        }
    }
}