namespace SourceMapper
{
    public interface ISource
    {
        string GetValue(string name, Position position = Position.Right, int offset = 1);
    }
}