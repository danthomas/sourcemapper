namespace SourceMapper
{
    public enum Position
    {
        None,
        Right,
        Below,
        Left,
        Above
    }
}