using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SourceMapper
{
    public class TableSource : ISource
    {
        private readonly string[,] _data;
        private readonly List<Name> _names;

        public TableSource(int width, int height)
        {
            _data = new String[width, height];
            _names = new List<Name>();
        }

        public string GetValue(string text, Position position = Position.Right, int offset = 1)
        {
            var name = _names.Single(z => z.Text == text);

            return _data[name.X, name.Y + offset];
        }

        public TableSource SetName(string text, int x, int y)
        {
            _names.Add(new Name { Text = text, X = x, Y = y});
            _data[x, y] = text;
            return this;
        }

        public TableSource SetValue(string text, string value, Position position = Position.Right, int offset = 1)
        {
            var name = _names.Single(z => z.Text == text);

            _data[name.X, name.Y + offset] = value;

            return this;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            for (int x = 0; x < _data.GetLength(0); ++x)
            {
                stringBuilder.AppendLine();
                for (int y = 0; y < _data.GetLength(1); ++y)
                {
                    stringBuilder.Append((y == 0 ? "" : ", ") + _data[x, y]);
                }
            }

            return stringBuilder.ToString();
        }

        class Name
        {
            public string Text { get; set; }
            public int X { get; set; }
            public int Y { get; set; }
        }
    }
}