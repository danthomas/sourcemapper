using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SourceMapper.Tests
{
    [TestClass]
    public class MapperTests : TestBase
    {
        [TestMethod]
        public void UseValue()
        {
            var source = new Source();

            var mapper = new Mapper<Thing>()
                .For(x => x.StringProp).UseValue("StringValue")
                .For(x => x.IntProp).UseValue(123)
                .For(x => x.BoolProp).UseValue(true)
                .For(x => x.SubThing.StringProp).UseValue("SubStringValue")
                .For(x => x.SubThing.IntProp).UseValue(456)
                .For(x => x.SubThing.BoolProp).UseValue(true)
                .Build();

            MapItemTest(mapper, source, @"{
  'StringProp': 'StringValue',
  'IntProp': 123,
  'BoolProp': true,
  'SubThing': {
    'StringProp': 'SubStringValue',
    'IntProp': 456,
    'BoolProp': true
  }
}");
        }

        [TestMethod]
        public void MapFrom()
        {
            var source = new Source()
                .SetValue("StringValue", "StringValue")
                .SetValue("IntValue", "123")
                .SetValue("BoolValue", "true")
                .SetValue("SubStringValue", "SubStringValue")
                .SetValue("SubIntValue", "456")
                .SetValue("SubBoolValue", "true");

            var mapper = new Mapper<Thing>()
                .For(x => x.StringProp).MapFromRightOf("StringValue")
                .For(x => x.IntProp).MapFromRightOf("IntValue")
                .For(x => x.BoolProp).MapFromRightOf("BoolValue")
                .For(x => x.SubThing.StringProp).MapFromRightOf("SubStringValue")
                .For(x => x.SubThing.IntProp).MapFromRightOf("SubIntValue")
                .For(x => x.SubThing.BoolProp).MapFromRightOf("SubBoolValue")
                .Build();

            MapItemTest(mapper, source, @"{
  'StringProp': 'StringValue',
  'IntProp': 123,
  'BoolProp': true,
  'SubThing': {
    'StringProp': 'SubStringValue',
    'IntProp': 456,
    'BoolProp': true
  }
}");
        }

        [TestMethod]
        public void WithDefault()
        {
            var source = new Source();

            var mapper = new Mapper<Thing>()
                .For(x => x.StringProp).MapFromRightOf("StringValue").WithDefault("StringValue")
                .For(x => x.IntProp).MapFromRightOf("IntValue").WithDefault(123)
                .For(x => x.BoolProp).MapFromRightOf("BoolValue").WithDefault(true)
                .For(x => x.SubThing.StringProp).MapFromRightOf("SubStringValue").WithDefault("SubStringValue")
                .For(x => x.SubThing.IntProp).MapFromRightOf("SubIntValue").WithDefault(456)
                .For(x => x.SubThing.BoolProp).MapFromRightOf("SubBoolValue").WithDefault(true)
                .Build();

            MapItemTest(mapper, source, @"{
  'StringProp': 'StringValue',
  'IntProp': 123,
  'BoolProp': true,
  'SubThing': {
    'StringProp': 'SubStringValue',
    'IntProp': 456,
    'BoolProp': true
  }
}");
        }

        [TestMethod]
        public void Pre()
        {
            var source = new Source()
                .SetValue("StringValue", "StringValue")
                .SetValue("IntValue", " 123 ")
                .SetValue("BoolValue", "Yes");

            var mapper = new Mapper<Thing>()
                .For(x => x.StringProp).MapFromRightOf("StringValue").Pre(x => x.ToLower())
                .For(x => x.IntProp).MapFromRightOf("IntValue").Pre(x => x.Trim())
                .For(x => x.BoolProp).MapFromRightOf("BoolValue").Pre(x => x == "Yes" ? "true" : "false")
                .Build();

            MapItemTest(mapper, source, @"{
  'StringProp': 'stringvalue',
  'IntProp': 123,
  'BoolProp': true
}");
        }

        [TestMethod]
        public void Post()
        {
            var source = new Source()
                .SetValue("StringValue", "StringValue")
                .SetValue("IntValue", "123")
                .SetValue("BoolValue", "false");

            var mapper = new Mapper<Thing>()
                .For(x => x.StringProp).MapFromRightOf("StringValue").Post(x => x.ToUpper())
                .For(x => x.IntProp).MapFromRightOf("IntValue").Post(x => x * 10)
                .For(x => x.BoolProp).MapFromRightOf("BoolValue").Post(x => !x)
                .Build();

            MapItemTest(mapper, source, @"{
  'StringProp': 'STRINGVALUE',
  'IntProp': 1230,
  'BoolProp': true
}");
        }

        [TestMethod]
        public void PreProcessing()
        {
            var source = new Source()
                .SetValue("FtpHost", " SFTP://123.456.789.1 ")
                .SetValue("IntValue", "  123  ")
                .SetValue("BoolValue", "false");

            var mapper = new Mapper<Thing>()
                .For(x => x.StringProp)
                    .MapFromRightOf("FtpHost")
                        .ToLower()
                        .Trim()
                        .RemoveLeading("ftp://", "sftp://")
                .Build();

            MapItemTest(mapper, source, @"{
  'StringProp': '123.456.789.1',
  'IntProp': 0,
  'BoolProp': false
}");
        }

        [TestMethod]
        public void MapItems()
        {
            var source = new TableSource(10, 10)
                .SetName("StringValue", 0, 0)
                .SetName("IntValue", 4, 0)
                .SetName("BoolValue", 7, 0)
                .SetValue("StringValue", "ABC", Position.Below)
                .SetValue("IntValue", "123", Position.Below)
                .SetValue("BoolValue", "false", Position.Below)
                .SetValue("StringValue", "DEF", Position.Below, 2)
                .SetValue("IntValue", "456", Position.Below, 2)
                .SetValue("BoolValue", "true", Position.Below, 2)
                .SetValue("StringValue", "GHI", Position.Below, 3)
                .SetValue("IntValue", "789", Position.Below, 3)
                .SetValue("BoolValue", "false", Position.Below, 3);
            
            var mapper = new Mapper<Thing>()
                .For(x => x.StringProp).MapFromBelow("StringValue")
                .For(x => x.IntProp).MapFromBelow("IntValue")
                .For(x => x.BoolProp).MapFromBelow("BoolValue")
                .Build();

            MapItemsTest(mapper, source, @"[
  {
    'StringProp': 'ABC',
    'IntProp': 123,
    'BoolProp': false
  },
  {
    'StringProp': 'DEF',
    'IntProp': 456,
    'BoolProp': true
  },
  {
    'StringProp': 'GHI',
    'IntProp': 789,
    'BoolProp': false
  }
]");
        }

        [TestMethod]
        public void Optional()
        {
            var source = new Source()
                .SetValue("StringValue", "");

            var mapper = new Mapper<Thing>()
                .For(x => x.StringProp).MapFromRightOf("StringValue").WithDefault("XYZ")
                .For(x => x.IntProp).MapFromRightOf("IntValue").Optional()
                .For(x => x.BoolProp).MapFromRightOf("BoolValue").WithDefault(true)
                .Build();

            MapItemTest(mapper, source, @"{
  'StringProp': 'XYZ',
  'IntProp': 0,
  'BoolProp': true
}");
        }

        [TestMethod]
        public void NullablProps()
        {
            var source = new Source()
                .SetValue("IntValue", "123")
                .SetValue("BoolValue", "true");

            var mapper = new Mapper<Thing>()
                .For(x => x.NullableIntProp).MapFromRightOf("IntValue")
                .For(x => x.NullableBoolProp).MapFromRightOf("BoolValue")
                .Build();

            MapItemTest(mapper, source, @"{
  'IntProp': 0,
  'BoolProp': false,
  'NullableIntProp': 123,
  'NullableBoolProp': true
}");
        }

        [TestMethod]
        public void NullNullablProps()
        {
            var source = new Source()
                .SetValue("IntValue", "");

            var mapper = new Mapper<Thing>()
                .For(x => x.NullableIntProp).MapFromRightOf("IntValue")
                .For(x => x.NullableBoolProp).MapFromRightOf("BoolValue")
                .Build();

            MapItemTest(mapper, source, @"{
  'IntProp': 0,
  'BoolProp': false
}");
        }

        [TestMethod]
        public void MissingNonOptionalNoDefaultException()
        {
            var source = new Source()
                .SetValue("StringValue", "");

            var mapper = new Mapper<Thing>()
                .For(x => x.StringProp).MapFromRightOf("StringValue")
                .Build();

            ExceptionTest(mapper, source, @"No value found for non optional non nullable StringValue mapping and no default set.");
        }
    }

    public class Thing
    {
        public string StringProp { get; set; }
        public int IntProp { get; set; }
        public bool BoolProp { get; set; }
        public int? NullableIntProp { get; set; }
        public bool? NullableBoolProp { get; set; }
        public SubThing SubThing { get; set; }
    }

    public class SubThing
    {
        public string StringProp { get; set; }
        public int IntProp { get; set; }
        public bool BoolProp { get; set; }
    }
}
